<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        DB::table('category')->insert([
            ['name' => 'Áo'],
            ['name' => 'Quần'],
            ['name' => 'Giày'],
            ['name' => 'Túi'],            
            ['name' => 'Mens'],            
            ['name' => 'Womens'],            
            ['name' => 'Clothing'],            
        ]);
        DB::table('brand')->insert([
            ['name' => 'Nike'],
            ['name' => 'Supreme'],
            ['name' => 'Addias'],
            ['name' => 'Channel'],            
            ['name' => 'H&M'],            
            ['name' => 'Gucci'],            
            ['name' => 'Louis Vuitton'],            
        ]);
    }
}
