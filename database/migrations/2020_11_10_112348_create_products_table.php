<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('price');
            $table->string('img');
            $table->integer('discount');
            $table->text('description');
            $table->bigInteger('id_brand')->unsigned();
            $table->foreign('id_brand')
            ->references('id')
            ->on('brand')
            ->onDelete('cascade');
            $table->bigInteger('id_category')->unsigned();
            $table->foreign('id_category')
            ->references('id')
            ->on('category')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
