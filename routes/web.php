<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect()->route('home');
});
Route::group([
    'namespace' => 'Frontend',
], function () {
    Route::get('/home', 'HomeController@home')->name('home');
    Route::get('/search', 'HomeController@search')->name('search');
    Route::get('/search_adv', 'HomeController@search_advanced')->name('search.adv');
    Route::post('/home', 'HomeController@search_price_range')->name('home');
    Route::get('/product/detail/{id}', 'HomeController@productDetail')->name('product.detail');
    Route::post('/product/detail', 'CartController@ajaxAddCart')->name('ajax.detail');
    Route::get('/cart', 'CartController@showCart')->name('cart.detail');
    Route::post('/cart/ajax', 'CartController@ajaxCart')->name('cart.ajax');
    Route::get('/cart/checkout', 'CartController@Checkout')->name('cart.checkout');
    Route::post('/cart/checkout', 'CartController@sendemail')->name('cart.sendmail');
    Route::get('/cart/complete', 'CartController@complete')->name('cart.complete');
    Route::get('/blog', 'BlogController@getBlog')->name('UI-blog');
    Route::get('/blog/detail/{id}', 'BlogController@show')->name('blog-detail');
    Route::post('/blog/detail/{id}', 'BlogController@postComment')->name('blog-comment');
    Route::get('/blog/detail', 'BlogController@ajaxGetRating')->name('blog-rating');
    Route::post('/blog/detail', 'BlogController@ajaxPostRating')->name('blog-rating');

    Route::group(['middleware' => 'memberNotLogin'], function () {
        Route::get('/member-login', 'MemberController@getLogin')->name('UI-login');
        Route::post('/member-login', 'MemberController@postLogin')->name('UI-login');
        Route::get('/member-signup', 'MemberController@getSignup')->name('UI-signup');
        Route::post('/member-signup', 'MemberController@postSignup')->name('UI-signup');
    });
    Route::group(['middleware' => 'member'], function () {
        Route::get('/logout', 'MemberController@logout')->name('UI-logout');
        Route::get('/profile/{id}', 'MemberController@getProfile')->name('UI-Profile');
        Route::post('/profile/{id}', 'MemberController@updateProfile')->name('UI-Profile');
        Route::get('/products/{id}', 'ProductController@getProducts')->name('UI-products');
        Route::get('/products/add/{id}', 'ProductController@getAddProducts');
        Route::post('/products/add/{id}', 'ProductController@postAddProducts');
        Route::get('/products/update/{id}', 'ProductController@update');
        Route::post('/products/update/{id}', 'ProductController@Postupdate');
        Route::get('/delete/products/{id}', 'ProductController@destroy');
    });
});

Auth::routes();
Route::group([
    'prefix' => 'Admin',
    'namespace' => 'Auth',
], function () {
    Route::get('/', 'LoginController@showLoginForm');
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login');
    Route::get('/logout', 'LoginController@logout')->name('logout');
});

Route::group([
    'prefix' => 'Admin',
    'namespace' => 'Admin',
    'middleware' => ['Admin']
], function () {
    Route::get('/home', 'HomeController@home');
    Route::get('/profile', 'UserController@getProfile')->name('profile');
    Route::post('/profile', 'UserController@postProfileUpdate')->name('profile');
    Route::get('/National', 'NationalController@index')->name('national');
    Route::post('/National', 'NationalController@create')->name('national');
    Route::get('/Blog', 'BlogController@index')->name('blog');
    Route::get('/Blog/add', 'BlogController@add')->name('add-blog');
    Route::post('/Blog/add', 'BlogController@create')->name('add-blog');
    Route::get('/Blog/edit/{id}', 'BlogController@edit')->name('editview-blog');
    Route::post('/Blog/edit/{id}', 'BlogController@update')->name('editview-blog');
    Route::get('/Blog/{id}', 'BlogController@destroy')->name('delete-blog');
});
