@extends('Admin.Layouts.master')
@section('content')
<div class="container-fluid">
    <div class="col-md-7">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Create Blog</h4>
            <h6 class="card-subtitle text-muted">Blog</h6>
            <form method="POST" class="form-horizontal m-t-30" enctype="multipart/form-data">
                @csrf
                @include('errors.error')
                @include('errors.notification')
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" value="">
                </div>
                <div class="form-group">
                    <label>Image</label>
                    <input type="file" name="image" class="form-control" multiple>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="description" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label>Content</label>
                    <textarea class="form-control" name="content" id="ckeditor" rows="5"></textarea>
                </div>
                <input type="submit" value="Add" class="btn btn-success">
                <a href="{{route('blog')}}" class="btn btn-warning">Cancel</a>
            </form>
          </div>
        </div>
    </div>
</div>
@endsection