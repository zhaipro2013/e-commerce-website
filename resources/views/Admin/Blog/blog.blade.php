@extends('Admin.Layouts.master')
@section('content')
<div class="col-12">
    <div class="card">
      <div class="card-body">
        <h4 class="page-title ">Contact</h4>
        <h6 class="card-subtitle">List contact</h6>
      </div>
      <div class="table-responsive">
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Title</th>
                    <th scope="col">Image</th>
                    <th scope="col">Description</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($blog as $item)
                <tr>
                    <td width="5%">{{$item->id}}</td>
                    <td width="15%">{{$item->title}}</td>
                    <td width="15%">{{$item->image}}</td>
                    <td width="40%">{{$item->description}}</td>
                    <td>
                        <div class="form-group">
                            <a href="{{route('editview-blog', ['id' => $item->id])}}" class="btn btn-info">Edit</a>
                            <a href="{{route('delete-blog', ['id' => $item->id])}}" class="btn btn-danger">Delete</a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<a href="{{route('add-blog')}}" class="btn btn-success">Add Blog</a>
</div>
@endsection