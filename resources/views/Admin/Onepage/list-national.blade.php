@extends('Admin.Layouts.master')
@section('content')
    <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card card-body">
                        <h4 class="card-title">Add quốc gia</h4>
                        <h5 class="card-subtitle">National</h5>
                        <form method="POST">
                            @csrf
                            @include('errors.notification')
                            <div class="form-group">
                                <label for="my-input">Quốc gia</label>
                                <input id="my-input" name="national" class="form-control" type="text">
                                <input type="submit" onclick="return confirm('Bạn có muốn thêm quốc gia này?')" class="form-control btn btn-primary" value="Add">
                            </div>
                        </form>
                    </div>
                </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Danh sách quốc gia</h4>
                                <h6 class="card-subtitle">List</h6>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">National</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($national as $item)
                                        <tr>
                                            <th scope="row">{{$item->id}}</th>
                                            <td>{{$item->country_name}}</td>
                                            <td><a class="btn btn-danger" href="">Delete</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
    </div>
@endsection