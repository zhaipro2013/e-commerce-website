@extends('Frontend.Layouts.master') 
@section('content')
    <div class="col-sm-4">
        <div class="signup-form">
            <!--sign up form-->
            <h2>User Signup!</h2>
            <form enctype="multipart/form-data" method="POST">
                @csrf
                @include('errors.error')
                @include('errors.notification')
                <div class="form-group">
                    <label class="col-md-12">Full Name</label>
                    <div class="col-md-12">
                        <input type="text" name="name" value="" class="form-control form-control-line" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="example-email" class="col-md-12">Email</label>
                    <div class="col-md-12">
                        <input type="email" name="email" value="" class="form-control form-control-line" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Password</label>
                    <div class="col-md-12">
                        <input type="password" name="password" value="" class="form-control form-control-line" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Phone Number</label>
                    <div class="col-md-12">
                        <input type="text" name="phone_Number" value="" class="form-control form-control-line" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Address</label>
                    <div class="col-md-12">
                        <input type="text" name="address" value="" class="form-control form-control-line" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Avatar</label>
                    <div class="col-md-12">
                        <input type="file" name="avatar" class="form-control form-control-line" multiple />
                    </div>
                </div>
                <button type="submit" style="margin-left: 15px" name="btn" class="btn btn-success">
                    Signup
                </button>
            </form>
        </div>
        <!--/sign up form-->
    </div>
@endsection