@extends('Frontend.Layouts.master')
@section('content')
		<div class="col-sm-9 padding-right">
			<div class="features_items">
				<!--features_items-->
				<h2 class="title text-center">User Update</h2>
				<div class="col-sm-8 col-sm-push-4" style="margin-bottom: 15px;">
					<div class="signup-form">
						<!--sign up form-->
						<form method="POST" enctype="multipart/form-data">
							@csrf
							@include('errors.error')
							@include('errors.notification')
							<input type="text" name="name" placeholder="Name" value="{{$profile['name']}}" />
							<input type="email" name="email" placeholder="Email Address" value="{{$profile['email']}}" />
							<input type="password" name="password" placeholder="Password" />
							<input type="text" name="phone_Number" placeholder="Phone Number" value="{{$profile['phone_Number']}}" />
							<input type="file" name="avatar" />
							<button type="submit" name="btn" class="btn btn-default">Update</button>
						</form>
					</div>
				</div>
			</div>
		</div>
@endsection

