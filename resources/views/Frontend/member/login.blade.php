@extends('Frontend.Layouts.master')
@section('content')
<div class="col-sm-9">
	<div class="blog-post-area">
		<h2 class="title text-center">Login</h2>
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						<form method="POST">
							@csrf
							@include('errors.error')
							@include('errors.notification')
							<input type="email" placeholder="email" name="email"/>
							<input type="password" placeholder="Password" name="password"/>
							<span>
								<input type="checkbox" class="checkbox"> 
								Keep me signed in
							</span>
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
			</div>
		</div>
	</div>
</div>	
<!--/form-->
@endsection

