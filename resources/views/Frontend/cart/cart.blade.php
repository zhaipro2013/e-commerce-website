@extends('Frontend.Layouts.master')
@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Shopping Cart</li>
				</ol>
			</div>
			@include('errors.error')
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						@if (Session::has('cart'))
							@foreach ($getProduct as $item)
							<tr>
								<td class="cart_product"><a href=""><img src="{{URL::to('upload/product/'.$item->user_id.'/'.$item->img[0])}}" alt="" width="150px" ></a></td>
								<td class="cart_description"><h4><a href="">{{$item->name}}</a></h4><p>Web ID: <span>{{$item->id}}</span></p></td>
								<td class="cart_price"><p>$<span> {{$item->price}}</span></p></td> 
								<td class="cart_quantity"> 
									<div class="cart_quantity_button"> 
										<a class="cart_quantity_up" style="cursor: pointer"> + </a> 
										<input class="cart_quantity_input" type="text" name="quantity" value="{{$item->qty}}" autocomplete="off" size="2">
										<a class="cart_quantity_down" style="cursor: pointer"> - </a>
									</div>
								</td>
								<td class="cart_total"><p class="cart_total_price">$<span>{{$item->price * $item->qty}}</span></p></td>
								<td class="cart_delete"><a class="cart_quantity_delete" style="cursor: pointer"><i class="fa fa-times"></i></a></td>
							</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</section>
	<!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your
					delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>

							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>

							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>$59</span></li>
							<li>Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>${{$total}}</span></li>
						</ul>
						<a class="btn btn-default update" href="">Update</a>
						<a class="btn btn-default check_out" href="{{route('cart.checkout')}}">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/#do_action-->
	
<script type="text/javascript">
$(document).ready(function () {
	var inputQty = 0;
	var total = 0;
	var price = 0;
	$(".cart_quantity_up").click(function () {
		var id = $(this).closest("tr").find(".cart_description p span").text();
		inputQty = $(this).next();
		var qty = inputQty.val();
		price = $(this).closest("tr").find(".cart_price p span").text();
		total = $(this).closest("tr").find(".cart_total p span");
		$.ajax({
			type: "POST",
			url: "{{url('cart/ajax')}}",
			data: { id: id, qty: qty, up: 1},
			success: function (response) {
				if(response.success){
					inputQty.val(response.qty);
					total.text(price * response.qty);
					$(".total_area ul li:last-child span").text("").append("$" + response.total);
				}
			}
		});
	});

	$(".cart_quantity_down").click(function () {
		var id = $(this).closest("tr").find(".cart_description p span").text();
		inputQty = $(this).prev();
		var qty = inputQty.val();
		price = $(this).closest("tr").find(".cart_price p span").text();
		total = $(this).closest("tr").find(".cart_total p span");
		$.ajax({
			type: "POST",
			url: "{{url('cart/ajax')}}",
			data: { id: id, qty: qty, down: 0},
			success: function (response) {
				if(response.success){
					inputQty.val(response.qty);
					total.text(price * response.qty);
					$(".total_area ul li:last-child span").text("").append("$" + response.total);
				}
			}
		});
	});

	$(".cart_quantity_delete").click(function (e) { 
        var id =$(this).closest("tr").find(".cart_description p span").text();
        var del = $(this);
		$.ajax({
			type: "POST",
			url: "{{url('cart/ajax')}}",
			data: { id: id, delete: true},
			success: function (response) {
				if(response.delete){
					del.closest("tr").hide();
					$(".total_area ul li:last-child span").text("").append("$" + response.total);
				}
			}
		});
    });
});

</script>
@endsection
