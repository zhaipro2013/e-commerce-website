@extends('Frontend.Layouts.master')
@section('content')
<section id="cart_items">
	<div class="container">
				<div class="breadcrumbs">
					<ol class="breadcrumb">
					  <li><a href="#">Home</a></li>
					  <li class="active">Check out</li>
					</ol>
				</div><!--/breadcrums-->
	
			@if (!Auth::check())
				<div class="step-one">
					<h2 class="heading">Step1</h2>
				</div>
				<div class="checkout-options">
					<h3>New User</h3>
					<p>Register before Order</p>
				</div><!--/checkout-options-->
				<div class="register">
					<div class="card">
						<div class="card-body">
							<form method="POST">
								@csrf
								@include('errors.error')
								@include('errors.notification')
								<div class="form-group">
									<label class="col-md-12">Full Name</label>
									<div class="col-md-12">
										<input type="text" name="name" value="" class="form-control form-control-line" />
									</div>
								</div>
								<div class="form-group">
									<label for="example-email" class="col-md-12">Email</label>
									<div class="col-md-12">
										<input type="email" name="email" value="" class="form-control form-control-line" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Password</label>
									<div class="col-md-12">
										<input type="password" name="password" value="" class="form-control form-control-line" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Phone Number</label>
									<div class="col-md-12">
										<input type="text" name="phone_Number" value="" class="form-control form-control-line" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Address</label>
									<div class="col-md-12">
										<input type="text" name="address" value="" class="form-control form-control-line" />
									</div>
								</div>
								<button type="submit" style="margin-left: 15px" name="btn" class="btn btn-success">
									Signup To Order
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="register-req">
					<p>Please use Register And Checkout to easily get access to your order history</p>
				</div><!--/register-req-->

				<div class="review-payment">
					<h2>Review & Payment</h2>
				</div>
			@endif

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description">Name Product</td>
							<td class="price" >Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>				
						</tr>
					</thead>
					<tbody>
						@if (Session::has('cart'))
							@foreach ($getProduct as $item)
							<tr>
								<td class="cart_product"><a href=""><img src="{{URL::to('upload/product/'.$item->user_id.'/'.$item->img[0])}}" alt="" width="150px" ></a></td>
								<td class="cart_description"><h4><a href="">{{$item->name}}</a></h4><p>Web ID: <span>{{$item->id}}</span></p></td>
								<td class="cart_price"><p>$<span> {{$item->price}}</span></p></td> 
								<td class="cart_quantity"> 
									<div class="cart_quantity_button"> 
										<a class="cart_quantity_up" style="cursor: pointer"> + </a> 
										<input class="cart_quantity_input" type="text" name="quantity" value="{{$item->qty}}" autocomplete="off" size="2">
										<a class="cart_quantity_down" style="cursor: pointer"> - </a>
									</div>
								</td>
								<td class="cart_total"><p class="cart_total_price">$<span>{{$item->price * $item->qty}}</span></p></td>
							</tr>
							@endforeach
						@endif
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>$59</td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>$2</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td><span>${{$total}}</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
				<form method="POST">@csrf <button type="submit" class="btn btn-primary Order" style="cursor: pointer">Order Products</button></form>
			</div>
</section> <!--/#cart_items-->
@endsection

	
