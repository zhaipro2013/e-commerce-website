<!DOCTYPE html>
<html lang="en">

<head>
	<base href="{{asset('')}}">
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Home | E-Shopper</title>
	<link href="frontend/css/bootstrap.min.css" rel="stylesheet" />
	<link href="frontend/css/font-awesome.min.css" rel="stylesheet" />
	<link href="frontend/css/prettyPhoto.css" rel="stylesheet" />
	<link href="frontend/css/price-range.css" rel="stylesheet" />
	<link href="frontend/css/animate.css" rel="stylesheet" />
	<link href="frontend/css/main.css" rel="stylesheet" />
	<link href="frontend/css/responsive.css" rel="stylesheet" />
	<link href="frontend/css/rates.css" rel="stylesheet" />
	@yield('css')
	<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	<link rel="shortcut icon" href="frontend/images/ico/favicon.ico" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="frontend/images/ico/apple-touch-icon-144-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="frontend/images/ico/apple-touch-icon-114-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="frontend/images/ico/apple-touch-icon-72-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" href="frontend/images/ico/apple-touch-icon-57-precomposed.png" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<!--/head-->

<body>
    @include('UILayouts.header')
	<!--/header-->
		@if (in_array('home', $actual_link))
			<section id="slider">
				<!--slider-->
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div id="slider-carousel" class="carousel slide" data-ride="carousel">
								<ol class="carousel-indicators">
									<li data-target="##slider-carousel" data-slide-to="0" class="active"></li>
									<li data-target="##slider-carousel" data-slide-to="1"></li>
									<li data-target="##slider-carousel" data-slide-to="2"></li>
								</ol>
		
								<div class="carousel-inner">
									<div class="item active">
										<div class="col-sm-6">
											<h1><span>E</span>-SHOPPER</h1>
											<h2>Free E-Commerce Template</h2>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit,
												sed do eiusmod tempor incididunt ut labore et dolore magna
												aliqua.
											</p>
											<button type="button" class="btn btn-default get">
												Get it now
											</button>
										</div>
										<div class="col-sm-6">
											<img src="images/home/girl1.jpg" class="girl img-responsive" alt="" />
											<img src="images/home/pricing.png" class="pricing" alt="" />
										</div>
									</div>
									<div class="item">
										<div class="col-sm-6">
											<h1><span>E</span>-SHOPPER</h1>
											<h2>100% Responsive Design</h2>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit,
												sed do eiusmod tempor incididunt ut labore et dolore magna
												aliqua.
											</p>
											<button type="button" class="btn btn-default get">
												Get it now
											</button>
										</div>
										<div class="col-sm-6">
											<img src="images/home/girl2.jpg" class="girl img-responsive" alt="" />
											<img src="images/home/pricing.png" class="pricing" alt="" />
										</div>
									</div>
		
									<div class="item">
										<div class="col-sm-6">
											<h1><span>E</span>-SHOPPER</h1>
											<h2>Free Ecommerce Template</h2>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit,
												sed do eiusmod tempor incididunt ut labore et dolore magna
												aliqua.
											</p>
											<button type="button" class="btn btn-default get">
												Get it now
											</button>
										</div>
										<div class="col-sm-6">
											<img src="images/home/girl3.jpg" class="girl img-responsive" alt="" />
											<img src="images/home/pricing.png" class="pricing" alt="" />
										</div>
									</div>
								</div>
		
								<a href="##slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								</a>
								<a href="##slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
									<i class="fa fa-angle-right"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>
		@endif
	<!--/slider-->
	@yield('ads')
	<section>
		<div class="container">
			<div class="row">
				@include('UILayouts.leftSidebar')

                @yield('content')
			</div>
		</div>
	</section>

	<footer id="footer">
		<!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2><span>e</span>-shopper</h2>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed
								do eiusmod tempor
							</p>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="##">
									<div class="iframe-img">
										<img src="images/home/iframe1.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>

						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="##">
									<div class="iframe-img">
										<img src="images/home/iframe2.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>

						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="##">
									<div class="iframe-img">
										<img src="images/home/iframe3.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>

						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="##">
									<div class="iframe-img">
										<img src="images/home/iframe4.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="images/home/map.png" alt="" />
							<p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Service</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="##">Online Help</a></li>
								<li><a href="##">Contact Us</a></li>
								<li><a href="##">Order Status</a></li>
								<li><a href="##">Change Location</a></li>
								<li><a href="##">FAQ’s</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Quock Shop</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="##">T-Shirt</a></li>
								<li><a href="##">Mens</a></li>
								<li><a href="##">Womens</a></li>
								<li><a href="##">Gift Cards</a></li>
								<li><a href="##">Shoes</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Policies</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="##">Terms of Use</a></li>
								<li><a href="##">Privecy Policy</a></li>
								<li><a href="##">Refund Policy</a></li>
								<li><a href="##">Billing System</a></li>
								<li><a href="##">Ticket System</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="##">Company Information</a></li>
								<li><a href="##">Careers</a></li>
								<li><a href="##">Store Location</a></li>
								<li><a href="##">Affillate Program</a></li>
								<li><a href="##">Copyright</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-sm-offset-1">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<form action="##" class="searchform">
								<input type="text" placeholder="Your email address" />
								<button type="submit" class="btn btn-default">
									<i class="fa fa-arrow-circle-o-right"></i>
								</button>
								<p>
									Get the most recent updates from <br />our site and be
									updated your self...
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">
						Copyright © 2013 E-SHOPPER Inc. All rights reserved.
					</p>
					<p class="pull-right">
						Designed by
						<span><a target="_blank" href="http://www.themeum.com">Themeum</a></span>
					</p>
				</div>
			</div>
		</div>
	</footer>
	<!--/Footer-->

	<script src="frontend/js/jquery.js"></script>
	<script src="frontend/js/bootstrap.min.js"></script>
	<script src="frontend/js/jquery.scrollUp.min.js"></script>
	<script src="frontend/js/price-range.js"></script>
	<script src="frontend/js/jquery.prettyPhoto.js"></script>
	<script src="frontend/js/main.js"></script>
	<script>
	$(document).ready(function () {
		$("a[rel^='prettyPhoto']").prettyPhoto()
		$('#comment').submit(function(){
		var loggedIn = "{{Auth::check()}}";
		if ( loggedIn == "" ){ 
			alert('Please to login before comment!');
			return false;
		}else
			return true;
		});
		$('.replay').click(function (e) { 
			$('#message').focus();
			$('#commentId').val($(this).attr('name'));
		});
	});
	</script>
	<script>
	
		$(document).ready(function(){
			//vote
			$('.ratings_stars').hover(
				// Handles the mouseover
				function() {
					$(this).prevAll().andSelf().addClass('ratings_hover');
					// $(this).nextAll().removeClass('ratings_vote'); 
				},
				function() {
					$(this).prevAll().andSelf().removeClass('ratings_hover');
					// set_votes($(this).parent());
				}
			);
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$('.ratings_stars').click(function(){
				var loggedIn = "{{Auth::check()}}";
				var inform = confirm("would you like submit this rating?");
				if (inform) {
					if ( loggedIn == "" ){ 
						alert('Please to login before rating blog!');
						return false;
					} else {
					$.ajax({
					type: "POST",
					url: "{{route('blog-rating')}}",
					data: {blog_id: $(this).nextAll('input').val(), rating_star:$(this).find('input').val()},
					dataType: "json",
					success: function (response) {
						$('.rate-np').text(response.success);
					}});
					var Values =  $(this).find("input").val();
					if ($(this).hasClass('ratings_over')) {
						$('.ratings_stars').removeClass('ratings_over');
						$(this).prevAll().andSelf().addClass('ratings_over');
					} else {
						$(this).prevAll().andSelf().addClass('ratings_over');
					}
						return true;
					}
					return true;
				} else {
					return false;
				}
			});
			//hide when browsing first time
			if($( "#status option:selected" ).text() == 'new'){
				$( ".percent" ).hide();
			}
			else {
				$( ".percent" ).show(300);
			}
			//check status with event change after browsing
			$('#status').change(function (e) {
				if($(this).val() == 0){
					$('.percent').show(300);
				} else {
					$('.percent').hide(300);
				}
			});

			$(".well").click(function (e) { 
					$.ajax({
						type: "post",
						url: "{{url("/home")}}",
						data: {price_range: $(".tooltip-inner").text()},
						dataType: "json",
						success: function (response) {
							if(!response.error){
								var productObj = response.success;
								var html = "";
								productObj.map(function(value, key){
									html += 	
									'<div class="col-sm-4">' +
										'<div class="product-image-wrapper">' +
											'<div class="single-products">' +
												'<div class="productinfo text-center">' +
													'<img src="{{ asset("upload/product") }}/'+ value.user_id +'/'+JSON.parse(value.img)[0] +'" alt="" />' +
													'<h2>$<span>'+ value.price +'</span></h2>' +
													'<p>'+ value.name +'</p>' +
													'<a href="{{ asset("/product/detail") }}/'+ value.id +'" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Xem chi tiết</a>' +
												"</div>" +
												'<div class="product-overlay">' +
													'<div class="overlay-content">' +
														'<h2>$<span>'+ value.price +'</span></h2>' +
														'<p>'+ value.name +'</p>' +
														'<a href="{{ asset("/product/detail") }}/'+ value.id +'" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Xem chi tiết</a>' +
													"</div>" +
												"</div>" +
											"</div>" +
											'<div class="choose">' +
												'<ul class="nav nav-pills nav-justified">' +
													'<li> <a href="##"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>' +
													'<li><a href="##"><i class="fa fa-plus-square"></i>Add to compare</a></li>' +
												"</ul>" +
											"</div>" +
										"</div>" +
									"</div>";
								});
								$(".features_items").find('.col-sm-4').remove();
								if($("div").hasClass('search-box')){
									$(html).insertAfter(".features_items .search-box");
								} else{
									$(html).insertAfter(".features_items .title");
								}
							} else{ 
								alert("Not found result");
							}
						}
					});
				});
			});
	</script>
</body>

</html>