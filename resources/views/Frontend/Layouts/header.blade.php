<header id="header">
    <!--header-->
    <div class="header_top">
        <!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li>
                                <a href="##"><i class="fa fa-phone"></i> +2 95 01 88 821</a>
                            </li>
                            <li>
                                <a href="##"><i class="fa fa-envelope"></i> info@domain.com</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="##"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="##"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="##"><i class="fa fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="##"><i class="fa fa-dribbble"></i></a>
                            </li>
                            <li>
                                <a href="##"><i class="fa fa-google-plus"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/header_top-->

    <div class="header-middle">
        <!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-md-4 clearfix">
                    <div class="logo pull-left">
                        <a href="index.html"><img src="images/home/logo.png" alt="" /></a>
                    </div>
                    <div class="btn-group pull-right clearfix">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle usa"
                                data-toggle="dropdown">
                                USA
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="">Canada</a></li>
                                <li><a href="">UK</a></li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle usa"
                                data-toggle="dropdown">
                                DOLLAR
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="">Canadian Dollar</a></li>
                                <li><a href="">Pound</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 clearfix">
                    <div class="shop-menu clearfix pull-right">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href=""><i class="fa fa-star"></i> Wishlist</a>
                            </li>
                            <li>
                                <a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a>
                            </li>
                            <li>
                                <a href="{{route('cart.detail')}}"><i class="fa fa-shopping-cart"></i>{{Session::has('cart')? count(Session::get('cart')) : 'Cart'}}</a>
                            </li>
                            @if (Auth::check())
                            <li>
                                <a href="{{route('UI-logout')}}"><i class="fa fa-lock"></i> Logout</a>
                            </li>
                            <li>
                                <a href="{{route('UI-Profile',['id' => Auth::id()])}}"><i class="fa fa-user"></i> Account</a>
                            </li>
                            @else
                            <li>
                                <a href="{{route('UI-login')}}"><i class="fa fa-lock"></i> Login</a>
                            </li>
                            <li>
								<a href="{{route('UI-signup')}}"><i class="fa fa-sign-in"></i> Sign Up</a>
							</li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/header-middle-->

    <div class="header-bottom">
        <!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{route('home')}}" class="active">Home</a></li>
                            <li class="dropdown">
                                <a href="##">Shop<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="shop.html">Products</a></li>
                                    <li>
                                        <a href="product-details.html">Product Details</a>
                                    </li>
                                    <li><a href="checkout.html">Checkout</a></li>
                                    <li><a href="cart.html">Cart</a></li>
                                    <li><a href="login.html">Login</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="{{route('UI-blog')}}">Blog<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="blog.html">Blog List</a></li>
                                    <li><a href="blog-single.html">Blog Single</a></li>
                                </ul>
                            </li>
                            <li><a href="404.html">404</a></li>
                            <li><a href="contact-us.html">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="search_box pull-right">
                        <form action="{{route('search')}}" id="search-form" method="GET">
                            <input type="text" name="keyword" placeholder="Search" />
                            <button type="submit" class="btn btn-info">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/header-bottom-->
    <script type="text/javascript">
        $("#search-form").submit(function (e) { 
            var keyword = $(this).find('input').val();
            if(keyword.length !== 0)
            {
                return true;
            }
            return false;
        });
    </script>
</header>