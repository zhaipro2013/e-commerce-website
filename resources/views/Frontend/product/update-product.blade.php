@extends('Frontend.Layouts.master')
@section('css')
<link href="frontend/css/product-update.css" rel="stylesheet" />
@endsection
@section('content')
<div class="col-sm-8 padding-right">
	<div class="features_items" style="margin-bottom: 15px"><!--features_items-->
		<h2 class="title text-center">Update product</h2>
		
		<div class="signup-form">
			<!--sign up form-->
			<div class="card">
				<div class="card-body">
					<!--sign up form-->
					<form enctype="multipart/form-data" method="POST">
						@csrf
						@include('errors.error')
						@include('errors.notification')
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" name="name" value="{{$product['name']}}" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" name="price" value="{{$product['price']}}" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<select class="form-control form-control-line" name="id_category" id="category">
									@foreach ($categories as $category)
										<option @if ($category->id == $product['id_category']) selected @endif value="{{$category->id}}">{{$category->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<select class="form-control form-control-line" name="id_brand" id="brand">
									@foreach ($brands as $brand)
									<option @if ($brand->id == $product['id_brand']) selected @endif value="{{$brand->id}}">{{$brand->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<select class="form-control form-control-line" name="status" id="status">
									<option @if ($product['status'] == 1) selected @endif value="1">new</option>
									<option @if ($product['status'] == 0) selected @endif value="0">sale</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<span class="percent"><input type="text" name="discount" value="{{ $product['discount'] }}"/>%</span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="file" name="img[]" class="form-control form-control-line" multiple>
							</div>
						</div>
						<div class="form-group">
							<label>Chọn hình muốn sữa</label>
							<ul class="img-checkbox">
								@for ($i = 0; $i < count($product['img']); $i++)
									<li><img src="{{ URL::to('upload/product/'.Auth::id().'/image80_'.$product['img'][$i]) }}" alt=""><input type="checkbox" class="item-checkbox" name="checkboxImg[]" value="{{ $product['img'][$i] }}"></li>
								@endfor
							</ul>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control" name="description" rows="5">{{$product['description']}}</textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<button type="submit" name="btn" class="btn btn-default">Update</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
