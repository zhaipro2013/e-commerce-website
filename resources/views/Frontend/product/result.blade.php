@extends('Frontend.Layouts.master')
@section('ads')
<section id="advertisement">
	<div class="container">
		<img src="Frontend/images/shop/advertisement.jpg" alt="" />
	</div>
</section>
@endsection
@section('content')
<div class="col-sm-9 padding-right">
	<div class="features_items"><!--features_items-->
		<h2 class="title text-center">Features Items</h2>
		<div class="search-box">
			<form action="{{route("search.adv")}}" class="search-advanced" method="GET">
				<div class="form-group">
				  <input type="text" name="name" value="<?= isset($_GET['name']) ? $_GET['name'] : ''?>" class="form-control item-search" placeholder="Name">
				</div>
				<div class="form-group">
				  <select class="form-control item-search" name="price">
					<option value="all">Choose price</option>
					@for ($i = 500; $i < 5000; $i+=1000)
						<option value="{{$i}}" <?= isset($_GET['price'])? (($_GET['price'] == $i) ? "selected" : "") : ""?>>{{$i}} > {{$i+1000}}$</option>
					@endfor
					</select>
				</div>
				<div class="form-group">
				  <select class="form-control item-search" name="category">
					<option value="all">category</option>
					@foreach ($categories as $category)
						<option value="{{$category->id}}" <?= isset($_GET['category']) ? (($_GET['category'] == $category->id) ? "selected" : "") : "" ?>>{{$category->name}}</option>
					@endforeach
				  </select>
				</div>
				<div class="form-group">
				  <select class="form-control item-search" name="brand">
					<option value="all">brand</option>
					@foreach ($brands as $brand)
						<option value="{{$brand->id}}" <?= isset($_GET['brand']) ? (($_GET['brand'] == $brand->id) ? "selected" : "") : ""?>>{{$brand->name}}</option>
					@endforeach
				  </select>
				</div>
				<div class="form-group">
				  <select class="form-control item-search" name="status">
					<option value="1">New</option>
					<option value="0">Sale</option>
				  </select>
				</div>
				<input type="submit" value="Search" class="btn btn-info" style="height: 100%">
			</form>
		</div>
		@if ($result->isEmpty())
			<h3 class="alert alert-danger">Không tìm thấy sản phẩm nào</h3>
		@else
			@foreach ($result as $Product)
				<div class="col-sm-4">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<img src="{{URL::to('upload/product/'.$Product->user_id.'/'.json_decode($Product->img)[0])}}" alt="" />
								<h2>$<span>{{ $Product->price }}</span></h2>
								<p>{{ $Product->name }}</p>
								<a href="{{ route('product.detail', ['id' => $Product->id]) }}" class="btn btn-default add-to-cart"><i
										class="fa fa-shopping-cart"></i>Xem chi tiết</a>
							</div>
							<div class="product-overlay">
								<div class="overlay-content">
									<h2>$<span>{{ $Product->price }}</span></h2>
									<p>{{ $Product->name }}</p>
									<a href="{{ route('product.detail', ['id' => $Product->id]) }}" class="btn btn-default add-to-cart"><i
											class="fa fa-shopping-cart"></i>Xem chi tiết</a>
								</div>
							</div>
						</div>
						<div class="choose">
							<ul class="nav nav-pills nav-justified">
								<li>
									<a href="##"><i class="fa fa-plus-square"></i>Add to wishlist</a>
								</li>
								<li>
									<a href="##"><i class="fa fa-plus-square"></i>Add to compare</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			@endforeach
			<ul class="pagination">
				<li class="active"><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">&raquo;</a></li>
			</ul>
		@endif

		
	</div><!--features_items-->
</div>
@endsection

	
