@extends('Frontend.Layouts.master')
@section('content')
	<div class="col-sm-8 padding-right">
		<div class="features_items" style="margin-bottom: 15px"><!--features_items-->
			<h2 class="title text-center">Create product</h2>
				
				<div class="signup-form">
					<!--sign up form-->
					<div class="card">
						<div class="card-body">
							<!--sign up form-->
							<form enctype="multipart/form-data" method="POST">
								@csrf
								@include('errors.error')
								@include('errors.notification')
								<div class="form-group">
									<div class="col-sm-12">
										<input type="text" name="name" placeholder="Name" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<input type="text" name="price" placeholder="price" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<select class="form-control form-control-line" name="id_category" id="category">
											@foreach ($categories as $category)
												<option value="{{$category->id}}">{{$category->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<select class="form-control form-control-line" name="id_brand" id="brand">
											@foreach ($brands as $brand)
												<option value="{{$brand->id}}">{{$brand->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<select class="form-control form-control-line" name="status" id="status">
											<option value="1">new</option>
											<option value="0">sale</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-2">
										<span class="percent"><input type="text" name="discount" value="0"/>%</span>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<input type="file" name="img[]" class="form-control form-control-line" multiple>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control" name="description" rows="5"></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<button type="submit" name="btn" class="btn btn-default">Create</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
	