@extends('Frontend.Layouts.master')
@section('content')
<div id="cart_items" class="col-sm-9 padding-right">
	<div class="features_items"><!--features_items-->
		<h2 class="title text-center">Product List</h2>
		<div class="col-md-12" style="margin-bottom: 15px;">
			<div class="table-responsive cart_info" style="margin-bottom: 0">
				<table class="table table-condensed">
						<thead>
							<tr class="cart_menu">
								<td class="id">id</td>
								<td class="name">name</td>
								<td class="image">image</td>
								<td class="price">Price</td>
								<td class="action" colspan="2">action</td>
							</tr>
						</thead>
						<tbody>
						@foreach ($product as $products)
							<tr>
								<td>{{$products->id}}</td>
								<td>{{$products->name}}</td>
								<td><img src="{{URL::to('upload/product/'.$products->user_id.'/image80_'.json_decode($products->img)[0])}}" alt=""></td>
								<td><span>{{$products->price}}</span>$</td>
								<td><a href="{{ url('/member/products/update/'.$products->id) }}" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
								<td><a href="{{ url('/member/delete/products/'.$products->id) }}" class="text-danger delete-Prod" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a></td>
							</tr>
						@endforeach	
						</tbody>
				</table>
			</div>
			<a href="{{url('/member/products/add/'.Auth::id())}}" class="btn btn-primary pull-right">Add</a>
		</div>
	</div>
</div>
@endsection	

	
