@extends('Frontend.Layouts.master')
@section('content')
<div class="col-sm-9">
	<div class="blog-post-area">
		<h2 class="title text-center">Latest From our Blog</h2>
		@foreach ($data as $item)
		<div class="single-blog-post">
			<h3>{{$item->title}}</h3>
			<div class="post-meta">
				<ul>
					<li><i class="fa fa-user"></i> Mac Doe</li>
					<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
					<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
				</ul>
				<div class="rate">
					<div class="vote">
						<div class="star_1 ratings_stars"><input value="1" type="hidden"></div>
						<div class="star_2 ratings_stars"><input value="2" type="hidden"></div>
						<div class="star_3 ratings_stars"><input value="3" type="hidden"></div>
						<div class="star_4 ratings_stars"><input value="4" type="hidden"></div>
						<div class="star_5 ratings_stars"><input value="5" type="hidden"></div>
						<span class="rate-np">4.5</span>
					</div> 
				</div>
			</div>
			<a href="">
				<img src="frontend/images/blog/{{$item->image}}" alt="">
			</a>
			<p>{{$item->description}}</p>
			<a  class="btn btn-primary" href="{{route('blog-detail',['id'=>$item->id])}}">Read More</a>
		</div>
		@endforeach
		<div class="pagination-area">
			<ul class="pagination">
				<li><a href="" class="active">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li><a href=""><i class="fa fa-angle-double-right"></i></a></li>
			</ul>
		</div>
	</div>
</div>	
@endsection

