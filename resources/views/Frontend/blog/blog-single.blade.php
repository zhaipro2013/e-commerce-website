@extends('Frontend.Layouts.master')
@section('content')
<div class="col-sm-9">
	<div class="blog-post-area">
		<h2 class="title text-center">Latest From our Blog</h2>
		<div class="single-blog-post">
			<h3>{{$getBlogDetail['title']}}</h3>
			<div class="post-meta">
				<ul>
					<li><i class="fa fa-user"></i> Mac Doe</li>
					<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
					<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
				</ul>
				<div class="rate">
					<div class="vote">
							@csrf
							@for ($i = 1; $i <= 5; $i++)
								<div class="star_{{$i}} ratings_stars @if($avg >= $i)ratings_over @endif"><input value="{{$i}}" name="star_{{$i}}" type="hidden"></div>
							@endfor
							
							<input value="{{$getBlogDetail['id']}}" name="blog_id" type="hidden">
							<span class="rate-np">{{$avg}}</span>
					</div> 
				</div>
			</div>
			<a href="">
				<img src="{{URL::to('frontend/images/blog/'.$getBlogDetail['image']) }} " alt="">
			</a>
			{!! $getBlogDetail['content'] !!}
			<div class="pager-area">
				<ul class="pager pull-right">
					<li><a href="#">Pre</a></li>
					<li><a href="#">Next</a></li>
				</ul>
			</div>
		</div>
	</div><!--/blog-post-area-->

	<div class="rating-area">
		<ul class="ratings">
			<li class="rate-this">Rate this item:</li>
			<li>
				<i class="fa fa-star color"></i>
				<i class="fa fa-star color"></i>
				<i class="fa fa-star color"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
			</li>
			<li class="color">(6 votes)</li>
		</ul>
		<ul class="tag">
			<li>TAG:</li>
			<li><a class="color" href="">Pink <span>/</span></a></li>
			<li><a class="color" href="">T-Shirt <span>/</span></a></li>
			<li><a class="color" href="">Girls</a></li>
		</ul>
	</div><!--/rating-area-->

	<div class="socials-share">
		<a href=""><img src="images/blog/socials.png" alt=""></a>
	</div><!--/socials-share-->

	<div class="media commnets">
		<a class="pull-left" href="#">
			<img class="media-object" src="images/blog/man-one.jpg" alt="">
		</a>
		<div class="media-body">
			<h4 class="media-heading">Annie Davis</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<div class="blog-socials">
				<ul>
					<li><a href=""><i class="fa fa-facebook"></i></a></li>
					<li><a href=""><i class="fa fa-twitter"></i></a></li>
					<li><a href=""><i class="fa fa-dribbble"></i></a></li>
					<li><a href=""><i class="fa fa-google-plus"></i></a></li>
				</ul>
				<a class="btn btn-primary" href="">Other Posts</a>
			</div>
		</div>
	</div><!--Comments-->
	<div class="response-area">
		<h2>3 RESPONSES</h2>
		<ul class="media-list">
	@foreach ($getBlogDetail['comment'] as $key => $value)
			@if ($value['level'] == 0)
				<li class="media">
					<a class="pull-left" href="#">
						<img class="media-object" src="images/blog/man-two.jpg" alt="">
					</a>
					<div class="media-body">
						<ul class="sinlge-post-meta">
							<li><i class="fa fa-user"></i>{{$value['username']}}</li>
							<li><i class="fa fa-clock-o"></i> {{date("D, d M Y", strtotime($value['created_at']))}}</li>
							<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
						</ul>
						<p>{{$value['txtcontent']}}</p>
						<button class="btn btn-primary replay" name="{{$value['id']}}"><i class="fa fa-reply"></i>Replay</button>
					</div>
				</li>				
			@endif
				@foreach ($getBlogDetail['comment'] as $comment)
					@if ($value['id'] == $comment['level'])
						<li class="media second-media">
							<a class="pull-left" href="#">
								<img class="media-object" src="images/blog/man-three.jpg" alt="">
							</a>
							<div class="media-body">
								<ul class="sinlge-post-meta">
									<li><i class="fa fa-user"></i>{{$comment['username']}}</li>
									<li><i class="fa fa-clock-o"></i> {{date("D, d M Y", strtotime($comment['created_at']))}}</li>
									<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
								</ul>
								<p>{{$comment['txtcontent']}}</p>
								<button class="btn btn-primary replay" name="{{$value['id']}}"><i class="fa fa-reply"></i>Replay</button>
							</div>
						</li>
					@endif
				@endforeach
		@endforeach
		</ul>					
	</div><!--/Response-area-->
	<div class="replay-box">
		<div class="row">
			<div class="col-sm-8">
				<h2>Leave a replay</h2>
				<form method="POST" id="comment">
					@csrf
					<div class="form-group text-area">
						<div class="blank-arrow">
							<label>Your comment</label>
						</div>
						<span>*</span>
					  <textarea id="message" name="message" rows="11"></textarea>
					  <input type="hidden" name="commentId" value="" id="commentId">
					</div>
					<input type="submit" value="post comment" class="btn btn-primary">
				</form>
			</div>
		</div>
	</div><!--/Repaly Box-->
</div>
@endsection



{{-- @foreach ($collection as $item)
	

	if $item(level) = 0 {
		<li></li>


		@foreach ($collection2 as $item2)
			if(item(id) == item2(level))
		}
	}
} --}}