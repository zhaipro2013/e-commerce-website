<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <style>
        </style>
    <font face = "Arial">
        <div>
            <div></div>
            <h3><font color="#FF9600">Thông tin khách hàng</font></h3>
            <p>
                <strong class="info">Khách hàng: {{$dataUser['name']}}</strong>
            </p>
            <p>
                <strong class="info">Email: {{$dataUser['email']}} </strong>
            </p>
            <p>
                <strong class="info">Điện thoại: {{$dataUser['phone_Number']}}</strong>
            </p>
            <p>
                <strong class="info">Địa chỉ: {{$dataUser['address']}}</strong>
            </p>
        </div>	
        <div class="table-responsive cart_info">
            <h3><font color="#FF9600">Hóa đơn mua hàng</font></h3>	
            <table class="table table-condensed" border="1" cellspacing="0">
                <thead>
                    <tr class="cart_menu">
                        <td class="description" width="30%">Name Product</td>
                        <td class="price" width="15%">Price</td>
                        <td class="quantity"width="10%">Quantity</td>
                        <td class="total"width="20%">Total</td>
                    </tr>
                </thead>
                <tbody>
                    @if (Session::has('cart'))
                        @foreach ($getProduct as $item)
                        <tr>
                            <td class="cart_description"><h4><a href="">{{$item->name}}</a></h4><p>Web ID: <span>{{$item->id}}</span></p></td>
                            <td class="cart_price"><p>$<span> {{$item->price}}</span></p></td> 
                            <td class="cart_quantity"> 
                                <div class="cart_quantity_button"> 
                                    <input class="cart_quantity_input" type="text" name="quantity" value="{{$item->qty}}" autocomplete="off" size="2">
                                </div>
                            </td>
                            <td class="cart_total"><p class="cart_total_price">$<span>{{$item->price * $item->qty}}</span></p></td>
                        </tr>
                        @endforeach
                    @endif
                    <tr>
                        <td colspan="4">&nbsp;</td>
                        <td colspan="2">
                            <table class="table table-condensed total-result">
                                <tr>
                                    <td>Cart Sub Total</td>
                                    <td>$59</td>
                                </tr>
                                <tr>
                                    <td>Exo Tax</td>
                                    <td>$2</td>
                                </tr>
                                <tr class="shipping-cost">
                                    <td>Shipping Cost</td>
                                    <td>Free</td>										
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td><span>${{$total}}</span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </font>
</body>
</html>