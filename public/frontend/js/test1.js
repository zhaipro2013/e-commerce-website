$(document).ready(function () {
    var prodList = localStorage.getItem("demo_2") ? JSON.parse(localStorage.getItem("demo_2")) : {};
    var id = 1;
    var sum = 0;
    //kiem tra prodList != {} hay khong
    if (!jQuery.isEmptyObject(prodList))
        //gan id = last id trong store
        for (id in prodList) { id; }
    Object.keys(prodList).map(function (key, index) {
        sum = sum + prodList[key]["quantity"];
        $(".shop-menu ul li a[href$='cart.html']").text("").append('<i class="fa fa-shopping-cart"></i>' + sum);
    });
    $(".add-to-cart").click(function () {
        var src = $(this).closest(".single-products").find("img").attr("src");
        var name = $(this).prevAll("p").text();
        var price = $(this).prevAll("h2").find("span").text();
        var qty = 1;
        var sum_qty = 1;
        var product = {};
        var flag = true;
        product["prod_name"] = name;
        product["price"] = price;
        product["quantity"] = qty;
        Object.keys(prodList).map(function (key, index) {
            sum_qty = sum_qty + prodList[key]["quantity"];
            //check xem img da co trong store hay chua
            if (src === prodList[key]["img"]) {
                //tra ve neu product da co trong store va update lai quantity
                qty = qty + prodList[key]["quantity"];
                prodList[key]["quantity"] = qty;
                flag = false;
            }
        });
        if (jQuery.isEmptyObject(prodList)) {
            product["id"] = id;
            product["img"] = src;
            prodList[id] = product;
        } else if (flag) {//neu la product new thi add product
            id++;
            product["id"] = id;
            product["img"] = src;
            prodList[id] = product;
        }
        $(".shop-menu ul li a[href$='cart.html']").text("").append('<i class="fa fa-shopping-cart"></i>' + sum_qty);
        localStorage.setItem("demo_2", JSON.stringify(prodList));
    });
});
