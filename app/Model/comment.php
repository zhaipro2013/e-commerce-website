<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class comment extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'comment';

    protected $fillable = [
        'username', 'email', 'txtcontent', 'level', 'user_id', 'blog_id'
    ];
}
