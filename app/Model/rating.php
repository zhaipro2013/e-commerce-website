<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class rating extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'rating';

    protected $fillable = [
        'vote', 'avg_rate', 'user_id', 'blog_id'
    ];
}
