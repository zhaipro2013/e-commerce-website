<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class history extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'history';

    protected $fillable = [
        'email', 'phone_Number', 'name', 'id_user', 'price'
    ];
}
