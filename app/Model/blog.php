<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class blog extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'blog_db';

    protected $fillable = [
        'title', 'image', 'description', 'content'
    ];
    public function comment() {
        return $this->hasMany('App\Model\comment', 'blog_id');
    }
    public function rating() {
        return $this->hasMany('App\Model\rating', 'blog_id');
    }
}
