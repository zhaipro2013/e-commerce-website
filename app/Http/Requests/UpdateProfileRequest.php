<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:32',
            'email' => 'email',
            'phone_Number' => 'max:10'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Username không được để trống',
            'email.email' => 'email không đúng định dạng',
            'phone_Number.max' => 'SĐT tối đa 10 kí tự',
        ];
    }
}
