<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddBlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required',
            'content' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'title không được để trống',
            'image.required' => 'img không được để trống',
            'image.image' => 'img không đúng định dạng',
            'description.required' => 'description không được để trống',
            'content.required' => 'content không được để trống'
        ];
    }
}
