<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'img' => 'required',
            'description' => 'required',
            'price' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'name không được để trống',
            'img.required' => 'img không được để trống',
            'description.required' => 'description không được để trống',
            'price.required' => 'price không được để trống'
        ];
    }
}
