<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AddBlogRequest;
use App\Model\blog;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = blog::all();
        return view('Admin.Blog.blog', compact('blog'));
    }
    public function add()
    {
        return view('Admin.Blog.AddBlog');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(AddBlogRequest $request)
    {
        $file = $request->image;
        $blog = new blog();
        $blog->title = $request->title;
        $blog->description = $request->description;
        $blog->content = $request->content;
        if(!empty($file)){
            $blog->image = $file->getClientOriginalName();
        }
        if ($blog->save()) {
            if(!empty($file)){
                $file->storeAs('images/blog', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success', __('Add blog success.'));
        } else {
            return redirect()->back()->withErrors('Add blog error.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = blog::findOrFail($id);
        return view('Admin.Blog.EditBlog', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = blog::findOrFail($id);
        $data = $request->all();
        $file = $request->image;
        if(!empty($file)){
            $data['image'] = $file->getClientOriginalName();
        }
        if ($blog->update($data)) {
            if(!empty($file)){
                $file->storeAs('images/blog', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success', __('Edit blog success.'));
        } else {
            return redirect()->back()->withErrors('Edit blog error.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        blog::destroy($id);
        return redirect()->back();
    }
}
