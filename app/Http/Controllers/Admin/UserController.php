<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfileRequest;
use App\User;
use App\Model\National;
use Auth;

class UserController extends Controller
{
    public function index()
    {
        return view('Admin.Home.index');
    }
    public function getProfile()
    {
        $data['user'] = User::find(Auth::id());
        $data['national'] = National::all();
        return view('Admin.User.profile', $data);
    }
    public function postProfileUpdate(UpdateProfileRequest $request)
    {
        $userId = Auth::id();
        $user = User::findOrFail($userId);

        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $file = $request->avatar;
        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }
        if ($user->update($data)) {
            if(!empty($file)){
                $file->move('assets/images/users', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success', __('Update profile success.'));
        } else {
            return redirect()->back()->withErrors('Update profile error.');
        }
    }
}
