<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\products;
use DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $product = products::all();
        return view('Frontend.home.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function productDetail($id)
    {
        $getProductDetail = products::FindOrFail($id)->toArray();
        $getProductDetail['img'] = json_decode($getProductDetail['img']);
        $productRecommend = DB::table('products')->orderBy('price','desc')->take(3)->get()->toArray();
        return view('Frontend.product.product-detail', compact('getProductDetail','productRecommend'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function search(Request $request)
    {
        $categories = DB::table('category')->get();
        $brands = DB::table('brand')->get();
        $result = DB::table('products')->where('name', 'like', '%' . str_replace(' ', '%', $request->keyword) . '%')->get();
        return view('Frontend.product.result', compact('result', 'categories', 'brands'));
    }

    public function search_advanced(Request $request)
    {
        $categories = DB::table('category')->get();
        $brands = DB::table('brand')->get();
        $result = DB::table('products');
        if (!empty($request->name)) {
            $result = $result->where('name', 'like', '%' . str_replace(' ', '%', $request->name) . '%');
        }
        if ($request->price != 'all') {
            $result = $result->whereBetween('price',  [$request->price, $request->price + 1000]);
        }
        if ($request->category != 'all') {
            $result = $result->where('id_brand', $request->category);
        }
        if ($request->brand != 'all') {
            $result = $result->where('id_brand', $request->brand);
        }
        if (!empty($request->status)) {
            $result = $result->where('status', $request->status);
        }
        $result = $result->get();
        return view('Frontend.product.result', compact('result', 'categories', 'brands'));
    }

    public function search_price_range(Request $request)
    {
        $str = str_replace(" :", "", $request->price_range);
        $min_price = (int) explode(" ", $str)[0];
        $max_price = (int) explode(" ", $str)[1];
        $result = DB::table('products')->whereBetween('price', [$min_price, $max_price])->get()->toArray();
        if(!empty($result)){
            return response()->json(['success' => $result]);
        } else {
            return response()->json(['error' => true]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
