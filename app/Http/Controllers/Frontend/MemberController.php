<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\SignupRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\User;
use Auth;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSignup()
    {
        return view('Frontend.member.signup');
    }
    public function getLogin()
    {
        return view('Frontend.member.login');
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postSignup(SignupRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->address = $request->address;
        $user->level = 0;
        $file = $request->avatar;

        if (!empty($file)) {
            $user->avatar = $file->getClientOriginalName();
        }
        if ($user->save()) {
            if (!empty($file)) {
                $file->storeAs('images/avatar', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success', __('Sign up success.'));
        } else {
            return redirect()->back()->withErrors('sign up error.');
        }
    }

    public function postLogin(Request $request)
    {
        $login = [
            'email' => $request->email,
            'password' => $request->password,
            'level' => 0
        ];

        $remember = false;

        if ($request->remember_me) {
            $remember = true;
        }


        if (Auth::attempt($login, $remember)) {
            return redirect()->route('home');
        } else {
            return redirect()->back()->withErrors('Email or password is not correct.');
        }
    }
    public function getProfile()
    {
        $profile = User::where('id', Auth::id())->where('level', Auth::user()->level)->get()->toArray();
        $profile = $profile[0];
        return view('Frontend.member.account', compact('profile'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(UpdateProfileRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $data = $request->all();

        if (!empty($data['password'])) {
            $data['password'] = bcrypt($request->password);
        } else {
            $data['password'] = Auth::user()->password;
        }

        $file = $request->avatar;

        if (!empty($file)) {
            $data['avatar'] = $file->getClientOriginalName();
        }
        if ($user->update($data)) {
            if (!empty($file)) {
                $file->move('assets/images/users', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success', __('Update profile success.'));
        } else {
            return redirect()->back()->withErrors(__('Update profile error.'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
