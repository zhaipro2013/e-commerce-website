<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\products;
use App\Http\Requests\ProductRequest;
use Image;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProducts()
    {
        $product = DB::table('products')->where('user_id', Auth::id())->get();
        return view('Frontend.product.List-product', compact('product'));
    }
    public function getAddProducts()
    {
        $categories = DB::table('category')->get();
        $brands = DB::table('brand')->get();
        return view('Frontend.product.Add-product', compact('categories', 'brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postAddProducts(ProductRequest $request)
    {
        $requestArrImg = $request->img;
        if ($request->hasFile('img')) {

            if (count($requestArrImg) > 3) {
                return redirect()->back()->withErrors('Please choose maximum 3 images');
            } else if (count($requestArrImg) < 3 || count($requestArrImg) == 3) {
                if (!empty($this->handleImg($requestArrImg))) {
                    $product = new products();
                    $product->img = json_encode($this->handleImg($requestArrImg));
                    $product->name = $request->name;
                    $product->price = $request->price;
                    $product->id_category = $request->id_category;
                    $product->id_brand = $request->id_brand;
                    $product->user_id = Auth::id();
                    $product->status = $request->status;
                    if ($request->status == 0) {
                        $product->discount = $request->discount;
                    }
                    $product->description = $request->description;
                    $product->save();
                }
            }
        }

        return redirect()->back()->with('success', __('Add product success.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $categories = DB::table('category')->get();
        $brands = DB::table('brand')->get();
        $product = products::find($id)->toArray();
        $product['img'] = json_decode($product['img'], true);
        return view('Frontend.product.update-product', compact('categories', 'brands', 'product'));
    }


    public function Postupdate(Request $request, $id)
    {
        $product = products::findOrFail($id);
        $productImg = json_decode($product['img']);
        $data = $request->all();
        $requestArrImg = $request->img;
        if ($request->hasFile('img')) {

            if (count($requestArrImg) > 3) {
                return redirect()->back()->withErrors('Please choose maximum 3 images');
            } else if (count($requestArrImg) < 3 || count($requestArrImg) == 3) {
                $ArrImg = $this->handleImg($requestArrImg);
            }

            if (!empty($request->checkboxImg)) {
                foreach ($productImg as $key => $img) {

                    if (in_array($img, $request->checkboxImg)) {
                        //lấy url của img cần xóa bao gồm cả img resize
                        $path = public_path('upload/product/' . Auth::id() . '/' . $img);
                        $path_2 = public_path('upload/product/' . Auth::id() . '/image80_' . $img);
                        $path_3 = public_path('upload/product/' . Auth::id() . '/image300_' . $img);
                        if (file_exists($path)) {
                            unlink($path);
                            unlink($path_2);
                            unlink($path_3);
                        }
                        unset($productImg[$key]);
                    }
                }
            } else {
                return redirect()->back()->withErrors('Please choose images you want to delete');
            }
            //gộp mảng img update và img sau khi xóa item cũ
            $data['img'] = json_encode(array_merge($productImg, $ArrImg));
        }
        if ($product->update($data)) {
            return redirect()->back()->with('success', __('Update product success.'));
        } else {
            return redirect()->back()->withErrors(__('Update product error.'));
        }
    }


    public function handleImg($arrImg)
    {
        $ArrayImg = [];

        foreach ($arrImg as $image) {

            $image_1 = strtotime(date('Y-m-d H:i:s')) . '_' . $image->getClientOriginalName(); // large image
            $image_2 = 'image80_' . strtotime(date('Y-m-d H:i:s')) . '_' . $image->getClientOriginalName(); // small image
            $image_3 = 'image300_' . strtotime(date('Y-m-d H:i:s')) . '_' . $image->getClientOriginalName(); // medium image
            // image80_574574_abc.png ip
            // image80_754756_abc.png ss
            if (!is_dir('upload/product/' . Auth::id())) {
                // path does not exist
                mkdir('upload/product/' . Auth::id());
                $path = public_path('upload/product/' . Auth::id() . '/' . $image_1);
                $path2 = public_path('upload/product/' . Auth::id() . '/' . $image_2);
                $path3 = public_path('upload/product/' . Auth::id() . '/' . $image_3);
            } else {
                $path = public_path('upload/product/' . Auth::id() . '/' . $image_1);
                $path2 = public_path('upload/product/' . Auth::id() . '/' . $image_2);
                $path3 = public_path('upload/product/' . Auth::id() . '/' . $image_3);
            }

            Image::make($image->getRealPath())->save($path);
            Image::make($image->getRealPath())->resize('81', '82')->save($path2);
            Image::make($image->getRealPath())->resize('250', '300')->save($path3);

            $ArrayImg[] = $image_1;
        }
        return $ArrayImg;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = products::findOrFail($id);
        $productImg = json_decode($product['img']);
        foreach ($productImg as $key => $img) {
            //lấy url của img cần xóa bao gồm cả img resize
            $path = public_path('upload/product/' . Auth::id() . '/' . $img);
            $path_2 = public_path('upload/product/' . Auth::id() . '/image80_' . $img);
            $path_3 = public_path('upload/product/' . Auth::id() . '/image300_' . $img);
            if (file_exists($path)) {
                unlink($path);
                unlink($path_2);
                unlink($path_3);
            }
        }
        products::destroy($id);
        return redirect()->back();
    }
}
