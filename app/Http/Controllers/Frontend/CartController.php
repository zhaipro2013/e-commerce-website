<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Model\products;
use Illuminate\Http\Request;
use App\Model\history;
use App\User;
use Auth;
use Mail;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxAddCart(Request $request)
    {
        $cart = [];
        $flag = true;
        if (session()->has('cart')) {
            $cart = session()->get('cart');
            foreach ($cart as $key => $item) {
                if ($item['id'] == $request->id) {
                    $cart[$key]['qty'] += 1;
                    session()->put('cart', $cart);
                    $flag = false;
                    break;
                }
            }
            //in the case the product unavailable in session
            if ($flag) {
                session()->push('cart', $request->all());
            }
        } else {
            // in the case the first product add to cart
            session()->push('cart', $request->all());
        }
        return response()->json(['success' => true, 'total' => count(session()->get('cart'))]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function Checkout()
    {
        $getProduct = [];
        $total = 0;
        if (empty(session()->has('cart'))) {
            return back()->withErrors('Unavailable Products in Cart to Checkout');
        }
        if (session()->has('cart')) {
            $getProduct = $this->getProductSession($getProduct);
            $total = $this->calTotal($total);
            return view('Frontend.cart.checkout', compact('getProduct', 'total'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCart()
    {
        $getProduct = [];
        $total = 0;
        if (session()->has('cart')) {
            $getProduct = $this->getProductSession($getProduct);
            $total = $this->calTotal($total);
        }
        return view('Frontend.cart.cart', compact('getProduct', 'total'));
    }
    public function sendemail(Request $request)
    {
        $getProduct = [];
        $total = 0;
        if (session()->has('cart')) {
            $getProduct = $this->getProductSession($getProduct);
            $total = $this->calTotal($total);
            if (Auth::check()) {
                $dataUser = Auth::user()->toArray();
                $sendmail = $this->handleMail($getProduct,$total,$dataUser);              
                if($sendmail){
                    return redirect()->route('cart.complete');
                }
            } else {
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = bcrypt($request->password);
                $user->phone_Number = $request->phone_Number;
                $user->address = $request->address;
                $user->level = 0;
                if ($user->save()) {
                    $dataUser = $user->toArray();
                    $sendmail = $this->handleMail($getProduct,$total,$dataUser);
                    if($sendmail){
                        return redirect()->route('cart.complete');
                    }
                } else {
                    return redirect()->back()->withErrors('sign up error.');
                }
            }
        }else {
            return redirect()->route('home');
        }
    }

    public function handleMail($getProduct, $total, $dataUser)
    {   
        $history = new history();
        $history->email = $dataUser['email'];
        $history->phone_Number = $dataUser['phone_Number'];
        $history->name = $dataUser['name'];
        $history->id_user = $dataUser['id'];
        $history->price = $total;
        if($history->save()){
            Mail::send('Frontend.email.email', compact('getProduct', 'total', 'dataUser'), function ($message) use ($dataUser){
                $message->from('zhaipro2013@gmail.com', 'Hai pro');
                $message->to($dataUser['email'], 'Mohamed salad')->subject('Confirm Invoice');;
            });
            return true;
        }
    }

    public function complete()
    {
        session()->forget('cart');
        return view('Frontend.cart.complete');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajaxCart(Request $request)
    {
        if (session()->has('cart')) {
            $qty = 0;
            $total = 0;
            $cart = session()->get('cart');
            $delete = false;
            if (isset($request->up) && $request->up == 1) {
                $qty = ++$request->qty;
            } else if (isset($request->down) && $request->down == 0) {
                if ($request->qty > 1) {
                    $qty = --$request->qty;
                } else {
                    $qty = $request->qty;
                }
            }

            foreach ($cart as $key => $item) {
                if ($item['id'] == $request->id) {
                    $cart[$key]['qty'] = $qty;
                    session()->put('cart', $cart);
                }
            }

            if (isset($request->delete) && $request->delete) {
                $delete = $this->destroy($request->id);
            }
            $total = $this->calTotal($total);
        }
        return response()->json(['success' => true, 'qty' => $qty, 'total' => $total, 'delete' => $delete]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function calTotal($total)
    {
        if (session()->has('cart')) {
            //get session and rearrange array index
            $cart = array_values(session()->get('cart'));
            foreach ($cart as $key => $item) {
                $getProduct[] = products::find($item['id']);
                //check product sale or new
                if ($getProduct[$key]['status'] == 0) {
                    $getProduct[$key]['price'] -= ($getProduct[$key]['price'] * ($getProduct[$key]['discount'] / 100));
                }
                $total += ($getProduct[$key]['price'] * $item['qty']);
            }
        }
        return $total;
    }

    public function getProductSession($arrProduct)
    {
        //get session and rearrange array index
        $cart = array_values(session()->get('cart'));
        foreach ($cart as $key => $item) {
            $arrProduct[] = products::find($item['id']);
            if ($arrProduct[$key]['status'] == 0) {
                $arrProduct[$key]['price'] -= ($arrProduct[$key]['price'] * ($arrProduct[$key]['discount'] / 100));
            }
            $arrProduct[$key]['img'] = json_decode($arrProduct[$key]['img']);
            $arrProduct[$key]['qty'] = $item['qty'];
        }
        return $arrProduct;
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (session()->has('cart')) {
            $cart = session()->get('cart');
            foreach ($cart as $key => $item) {
                if ($item['id'] == $id) {
                    unset($cart[$key]);
                }
            }
            session()->put('cart', $cart);
        }
        $remainProduct = session()->get('cart');
        if (empty($remainProduct)) {
            session()->forget('cart');
        }
        return true;
    }
}
