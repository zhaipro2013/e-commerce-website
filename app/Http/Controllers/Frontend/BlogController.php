<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\blog;
use App\Model\comment;
use App\Model\rating;
use Auth;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBlog()
    {
        $data = blog::all();
        return view('Frontend.blog.blog', compact('data'));
    }
    public function postComment(Request $request, $id)
    {
        $blogId = $id;
        $comment = new comment();
        if (!empty($request->commentId)) {
            $comment->level = $request->commentId;
            $userId = Auth::id();
        } else {
            $userId = Auth::id();
            $comment->level = 0;
        }
        $comment->username = Auth::user()->name;
        $comment->email = Auth::user()->email;
        $comment->txtcontent = $request->message;
        $comment->user_id = $userId;
        $comment->blog_id = $blogId;
        $comment->save();
        return redirect()->back();
    }
    public function ajaxGetRating()
    {
        $data = blog::all();
        return view('Frontend.blog.blog', compact('data'));
    }
    public function ajaxPostRating(Request $request)
    {
        $blog_id = $request->blog_id;
        $value['vote_quantity'] = $request->rating_star;
        $checkRating = rating::where('user_id', Auth::id())->where('blog_id', $blog_id)->get()->toArray();
        //trường hợp người đã vote chọn vote lại lần thứ n
        if (!empty($checkRating)) {
            rating::where('user_id', Auth::id())->where('blog_id', $blog_id)->update($value);
        }
        //trường hợp người đầu tiên và người thứ n vote lần đầu 
        else {
            $newRating = new rating();
            $newRating->vote_quantity = $request->rating_star;
            $newRating->blog_id = $blog_id;
            $newRating->user_id = Auth::id();
            $newRating->save();
        }
        return response()->json(['success' => $request->rating_star]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getBlogDetail = blog::with(['comment' => function ($q) {
        }])->where('id', $id)->get()->toArray();

        $getBlogRating = blog::with(['rating' => function ($q) {
        }])->where('id', $id)->get()->toArray();

        $getBlogRating = $getBlogRating[0];
        $getBlogDetail = $getBlogDetail[0];
        $totalRating = 0;

        if (empty($getBlogRating['rating'])) {
            $avg = 0;
        } else {
            foreach ($getBlogRating['rating'] as $value) {
                $totalRating += $value['vote_quantity'];
            }
            $count = count($getBlogRating['rating']);
            $avg = round($totalRating / $count, 1);
        }
        return view('Frontend.blog.blog-single', compact('getBlogDetail', 'avg'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
