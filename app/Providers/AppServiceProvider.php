<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        if(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
            // $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            $actual_link = explode('/', $_SERVER['REQUEST_URI']);
        }
        if(isset($actual_link)){
            $search = 'null';
            if(isset($_GET['keyword'])){
                $search = $actual_link[3];
            }
            if(isset($_GET['name']) && isset($_GET['price']) && isset($_GET['status'])){
                $search = $actual_link[3];
            }
            $domain = ['home', 'blog', 'profile', 'products', 'detail', $search];
            View::share(compact('actual_link','domain'));
        }
    }
}
